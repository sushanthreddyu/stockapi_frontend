import React, { useState } from 'react'
import {
	Button,
	Divider,
	makeStyles,
	Paper,
	TextField,
	Typography,
} from '@material-ui/core'
import Link from 'next/link'

import { useDispatch } from 'react-redux'
import { loginUser } from '../../redux/modules/user/actions'

import { AuthToken } from '../../lib/services/AuthToken'

import { USERNAME } from '../../lib/consts'

const useStyles = makeStyles((theme) => ({
	root: {
		display: 'flex',
		flexDirection: 'column',
	},
	form: {
		display: 'flex',
		flexDirection: 'column',
		margin: theme.spacing(4),
		'& > *': {
			margin: theme.spacing(1),
		},
	},
	label: {
		padding: theme.spacing(2.5),
		background: theme.palette.primary.main,
		borderTopLeftRadius: 'inherit',
		borderTopRightRadius: 'inherit',
		color: theme.palette.primary.contrastText,
	},
	divider: {
		margin: theme.spacing(1),
	},
	register: {
		display: 'flex',
		justifyContent: 'center',
		alignItems: 'center',
		margin: theme.spacing(1),
	},
}))

const Login = () => {
	const dispatch = useDispatch()

	const classes = useStyles()

	const [username, setUsername] = useState('')
	const [password, setPassword] = useState('')

	const handleUsernameChange = (value: string) => {
		setUsername(value)
	}

	const handlePasswordChange = (value: string) => {
		setPassword(value)
	}

	const authUser = async () => {
		const user = {
			username: username,
			password: password,
		}
		let raw = JSON.stringify(user)

		let myHeaders = new Headers()
		myHeaders.append('Content-Type', 'application/json')

		fetch(`${process.env.NEXT_PUBLIC_SERVER_URL}/login`, {
			method: 'POST',
			headers: myHeaders,
			body: raw,
			redirect: 'follow',
		})
			.then((response) => {
				const jwt = response.headers.get('Authorization')
				localStorage.setItem(USERNAME, username)
				AuthToken.storeToken(jwt)
				return jwt
			})
			.then((jwt) => {
				if (jwt !== null && jwt.includes('Bearer')) {
					dispatch(loginUser())
				} else {
					alert('Incorrect Credentials')
					handleUsernameChange('')
					handlePasswordChange('')
				}
			})
			.catch((error) => {
				alert('Incorrect Credentials')
				handleUsernameChange('')
				handlePasswordChange('')
			})
	}

	return (
		<Paper className={classes.root}>
			<Typography variant='button' className={classes.label}>
				Member Login
			</Typography>

			<form className={classes.form} noValidate autoComplete='off'>
				<TextField
					id='outlined-basic'
					label='Username'
					variant='outlined'
					value={username}
					onChange={(event) =>
						handleUsernameChange(event.target.value)
					}
				/>
				<TextField
					id='outlined-basic'
					label='Password'
					variant='outlined'
					type='password'
					value={password}
					onChange={(event) =>
						handlePasswordChange(event.target.value)
					}
				/>
				<Button
					variant='contained'
					color='secondary'
					size='large'
					disableElevation
					onClick={() => authUser()}>
					Login
				</Button>
			</form>
			<Divider className={classes.divider} />
			<Typography className={classes.register}>
				Not a Member Yet?&nbsp;
				<Link href='/register'>
					<a>Register Now</a>
				</Link>
			</Typography>
		</Paper>
	)
}

export default Login
