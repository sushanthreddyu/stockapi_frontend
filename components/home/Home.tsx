import React, { useEffect, useState } from 'react'

import { Grid, makeStyles, Paper, Typography } from '@material-ui/core'

import Login from './Login'

const useStyles = makeStyles((theme) => ({
	root: {
		display: 'flex',
		flexGrow: 1,
	},
	control: {
		padding: theme.spacing(2),
	},
	left_label: {
		color: '#BDBDBD',
	},
	left_card: {
		padding: theme.spacing(4),
		background: '#424242',
	},
	left_grid: {
		background: '#2B6B75',
		display: 'flex',
		justifyContent: 'center',
		alignItems: 'center',
	},
	right_grid: {
		background: '#FFBA32',
		display: 'flex',
		justifyContent: 'center',
		alignItems: 'center',
	},
}))

const Home = () => {
	const classes = useStyles()

	return (
		<Grid container spacing={0} className={classes.root}>
			<Grid item xs={12} md={6} className={classes.left_grid}>
				<Paper className={classes.left_card} elevation={5}>
					<Typography className={classes.left_label} variant='h1'>
						Stock API
					</Typography>
				</Paper>
			</Grid>
			<Grid item xs={12} md={6} className={classes.right_grid}>
				<Login />
			</Grid>
		</Grid>
	)
}

export default Home
