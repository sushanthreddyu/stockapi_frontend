import React, { useEffect, useState } from 'react'

import { useSelector, useDispatch } from 'react-redux'
import { addUser, logoutUser } from '../../redux/modules/user/actions'

import { Grid } from '@material-ui/core'

import APIKey from './APIKey'
import UserDetails from './UserDetails'
import { TOKEN_STORAGE_KEY, USERNAME } from '../../lib/consts'

const Dashboard = () => {
	const user = useSelector((state: { user: any }) => state.user)
	const dispatch = useDispatch()

	useEffect(() => {
		const storedToken = localStorage.getItem(TOKEN_STORAGE_KEY)
		const storedUsername = localStorage.getItem(USERNAME)

		getUserData(storedUsername, storedToken)
	}, [])

	const getUserData = async (username: string, jwt: string) => {
		let myHeaders = new Headers()
		myHeaders.append('Authorization', jwt)

		fetch(
			`${process.env.NEXT_PUBLIC_SERVER_URL}/user?username=${username}`,
			{
				method: 'GET',
				headers: myHeaders,
				redirect: 'follow',
			}
		)
			.then((response) => response.json())
			.then((result) => dispatch(addUser(result)))
			.catch((error) => alert("Couldn't Fetch User Data"))
	}

	return (
		<Grid container style={{ flexGrow: 1 }}>
			<Grid item xs={12} md={4} style={{ flexGrow: 1 }}>
				<UserDetails user={user} />
			</Grid>
			<Grid item style={{ flexGrow: 1 }}>
				<APIKey user={user} />
			</Grid>
		</Grid>
	)
}

export default Dashboard
