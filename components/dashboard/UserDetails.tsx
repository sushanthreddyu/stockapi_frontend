import React from 'react'
import {
	Divider,
	List,
	ListItem,
	ListItemText,
	ListSubheader,
	makeStyles,
	Paper,
	Typography,
} from '@material-ui/core'
import { inherits } from 'util'

const useStyles = makeStyles((theme) => ({
	root: {
		margin: theme.spacing(2),

		backgroundColor: theme.palette.background.paper,
	},
	subheader: {
		padding: theme.spacing(2),
		borderTopLeftRadius: 'inherit',
		borderTopRightRadius: 'inherit',
		backgroundColor: theme.palette.secondary.main,
		color: theme.palette.secondary.contrastText,
	},
}))

const UserDetails = ({ user }) => {
	const classes = useStyles()
	return (
		<Paper className={classes.root}>
			<Typography className={classes.subheader}>User Details</Typography>
			<List>
				<ListItem>
					<ListItemText
						primary='Username'
						secondary={user.username}
					/>
				</ListItem>
				<Divider />
				<ListItem>
					<ListItemText primary='Email Id' secondary={user.email} />
				</ListItem>
				<Divider />
				<ListItem>
					<ListItemText
						primary='First Name'
						secondary={user.first_name}
					/>
				</ListItem>
				<Divider />
				<ListItem>
					<ListItemText
						primary='Last Name'
						secondary={user.last_name}
					/>
				</ListItem>
				<Divider />
				<ListItem>
					<ListItemText
						primary='Phone Number'
						secondary={user.phno}
					/>
				</ListItem>
				<Divider />
				<ListItem>
					<ListItemText
						primary='Account Type'
						secondary={user.account_type}
					/>
				</ListItem>
			</List>
		</Paper>
	)
}

export default UserDetails
