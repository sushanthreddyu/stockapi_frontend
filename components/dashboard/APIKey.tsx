import { makeStyles, Paper, TextField, Typography } from '@material-ui/core'
import React, { useEffect } from 'react'

const useStyles = makeStyles((theme) => ({
	root: {
		margin: theme.spacing(2),
		padding: theme.spacing(2),
		backgroundColor: theme.palette.background.paper,
	},
	subheader: {
		padding: theme.spacing(2),
		borderTopLeftRadius: 'inherit',
		borderTopRightRadius: 'inherit',
		backgroundColor: theme.palette.secondary.main,
		color: theme.palette.secondary.contrastText,
	},
}))

const APIKey = ({ user }) => {
	const classes = useStyles()
	return (
		<Paper className={classes.root}>
			<TextField
				fullWidth
				label='API KEY'
				value={user.api_key}
				color='secondary'
				variant='outlined'></TextField>
		</Paper>
	)
}

export default APIKey
