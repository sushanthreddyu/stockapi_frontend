import React, { ReactNode, useEffect } from 'react'

import Head from 'next/head'
import Link from 'next/link'
import Router from 'next/router'

import { useSelector, useDispatch } from 'react-redux'
import { addUser, logoutUser } from '../redux/modules/user/actions'

import {
	Button,
	AppBar,
	IconButton,
	Toolbar,
	makeStyles,
} from '@material-ui/core'

import { ExitToApp } from '@material-ui/icons'
import { TOKEN_STORAGE_KEY, USERNAME } from '../lib/consts'

const useStyles = makeStyles((theme) => ({
	root: {
		display: 'flex',
		flexDirection: 'column',
		height: 'inherit',
	},
	appBar: {
		background: 'black',
	},
	menuButton: {
		marginRight: theme.spacing(2),
	},
	logo: {
		marginRight: theme.spacing(1),
	},
	main: {
		display: 'flex',
		flexGrow: 1,
	},
}))

const Layout = ({ children }: { children: ReactNode }) => {
	const classes = useStyles()
	let isLoggedIn = useSelector(
		(state: { user: { isLoggedIn: boolean } }) => state.user.isLoggedIn
	)
	const dispatch = useDispatch()

	useEffect(() => {
		Router.prefetch('/docs')
	}, [])

	return (
		<>
			<div className={classes.root}>
				<Head>
					<title>Stock API</title>
					<link rel='icon' href='/assets/stock-market.svg' />
				</Head>
				<header>
					<AppBar position='static'>
						<Toolbar>
							<Button color='inherit' className={classes.logo}>
								<img
									width='45'
									height='45'
									src='/assets/stock-market.svg'></img>
							</Button>
							<Button
								color='inherit'
								style={{ marginRight: 'auto' }}
								onClick={() => Router.push('/docs')}>
								DOCS
							</Button>
							{isLoggedIn && (
								<IconButton
									color='inherit'
									onClick={() => dispatch(logoutUser())}>
									<ExitToApp />
								</IconButton>
							)}
						</Toolbar>
					</AppBar>
				</header>
				<main className={classes.main}>{children}</main>
			</div>
			<footer
				style={{
					height: '50px',
					display: 'flex',
					justifyContent: 'center',
					alignItems: 'center',
					background: 'black',
					color: 'white',
				}}>
				<span>{attributionText}</span>
			</footer>
		</>
	)
}

const attributionText = (
	<>
		Icons made by{' '}
		<a href='https://www.flaticon.com/authors/eucalyp' title='Eucalyp'>
			Eucalyp
		</a>{' '}
		from{' '}
		<a href='https://www.flaticon.com/' title='Flaticon'>
			{' '}
			www.flaticon.com
		</a>
	</>
)

export default Layout
