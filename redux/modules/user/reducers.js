import { TOKEN_STORAGE_KEY, USERNAME } from '../../../lib/consts'
import { ADD_USER, LOGIN_USER, LOGOUT_USER, REMOVE_USER } from './actionTypes'

const INITIAL_STATE = {
	username: '',
	email: '',
	first_name: '',
	last_name: '',
	phno: '',
	api_key: '',
	account_type: '',
	isLoggedIn: false,
}

const user = (state = INITIAL_STATE, action) => {
	switch (action.type) {
		case ADD_USER:
			const { data } = action.payload
			return {
				...state,
				username: data.username,
				email: data.email,
				first_name: data.first_name,
				last_name: data.last_name,
				phno: data.phno,
				api_key: data.apiKey,
				account_type: data.premium_user ? 'Premium' : 'Free',
				isLoggedIn: true,
			}

		case REMOVE_USER:
			return {
				...state,
				username: '',
				email: '',
				first_name: '',
				last_name: '',
				phno: '',
				api_key: '',
				isLoggedIn: false,
			}

		case LOGIN_USER:
			return {
				...state,
				isLoggedIn: true,
			}

		case LOGOUT_USER:
			localStorage.clear()
			return {
				...INITIAL_STATE,
			}

		default:
			return state
	}
}

export default user
