import { ADD_USER, REMOVE_USER, LOGIN_USER, LOGOUT_USER } from './actionTypes'

export const loginUser = () => ({
	type: LOGIN_USER,
})

export const logoutUser = () => ({
	type: LOGOUT_USER,
})

export const addUser = (data) => ({
	type: ADD_USER,
	payload: {
		data,
	},
})

export const removeUser = () => ({
	type: REMOVE_USER,
})
