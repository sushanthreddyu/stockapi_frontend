import {
	Button,
	Container,
	Paper,
	Typography,
	makeStyles,
	TextField,
	Select,
	MenuItem,
	FormControl,
	InputLabel,
} from '@material-ui/core'
import React, { useState } from 'react'
import Router from 'next/router'
import Layout from '../components/Layout'

const useStyles = makeStyles((theme) => ({
	root: {
		background: '#FFBA32',
		display: 'flex',
		justifyContent: 'center',
		alignItems: 'center',
		flexGrow: 1,
	},
	paper: {
		display: 'flex',
		flexDirection: 'column',
	},
	label: {
		padding: theme.spacing(2.5),
		backgroundColor: theme.palette.primary.main,
		color: theme.palette.primary.contrastText,
		borderTopLeftRadius: 'inherit',
		borderTopRightRadius: 'inherit',
	},
	form: {
		display: 'flex',
		flexDirection: 'column',
		margin: theme.spacing(4),
		'& > *': {
			margin: theme.spacing(1),
		},
	},
	formControl: {
		marginRight: theme.spacing(2),
	},
}))

export type LoginInputs = {
	username: string
	email: string
	password: string
	phno: string
	first_name: string
	last_name: string
	account_type: string
}

const register = () => {
	const classes = useStyles()

	const InitialValues: LoginInputs = {
		username: '',
		email: '',
		password: '',
		phno: '',
		first_name: '',
		last_name: '',
		account_type: '',
	}

	const [inputs, setInputs] = useState(InitialValues)

	const handleInputChange = (e: React.ChangeEvent<any>) => {
		e.persist()
		setInputs({
			...inputs,
			[e.target.name]: e.target.value,
		})
	}

	const registerUser = async () => {
		let myHeaders = new Headers()
		myHeaders.append('Content-Type', 'application/json')

		let raw = JSON.stringify({
			...inputs,
			premium_user: inputs.account_type === 'premium' ? true : false,
		})

		fetch(`${process.env.NEXT_PUBLIC_SERVER_URL}/register`, {
			method: 'POST',
			headers: myHeaders,
			body: raw,
			redirect: 'follow',
		})
			.then((response) => response.text())
			.then((result) => {
				alert('Account Created')
				Router.push('/')
			})
			.catch((error) => console.log('error', error))
	}

	return (
		<Layout>
			<div className={classes.root}>
				<Paper className={classes.paper}>
					<Typography variant='button' className={classes.label}>
						Register
					</Typography>
					<form
						className={classes.form}
						noValidate
						autoComplete='off'>
						<div>
							<FormControl>
								<TextField
									id='email'
									label='Email ID'
									name='email'
									variant='outlined'
									onChange={handleInputChange}
									value={inputs.email}
								/>
							</FormControl>
						</div>
						<div>
							<FormControl className={classes.formControl}>
								<TextField
									id='username'
									label='Username'
									name='username'
									variant='outlined'
									onChange={handleInputChange}
									value={inputs.username}
								/>
							</FormControl>
							<FormControl>
								<TextField
									id='password'
									label='Password'
									name='password'
									variant='outlined'
									type='password'
									onChange={handleInputChange}
									value={inputs.password}
								/>
							</FormControl>
						</div>
						<div>
							<FormControl className={classes.formControl}>
								<TextField
									id='first_name'
									label='First Name'
									name='first_name'
									variant='outlined'
									onChange={handleInputChange}
									value={inputs.first_name}
								/>
							</FormControl>
							<FormControl>
								<TextField
									id='last_name'
									label='Last Name'
									name='last_name'
									variant='outlined'
									onChange={handleInputChange}
									value={inputs.last_name}
								/>
							</FormControl>
						</div>
						<div>
							<FormControl>
								<TextField
									id='phno'
									label='Phone Number'
									name='phno'
									variant='outlined'
									onChange={handleInputChange}
									value={inputs.phno}
								/>
							</FormControl>
						</div>
						<div>
							<FormControl
								variant='outlined'
								style={{ minWidth: '150px' }}>
								<InputLabel id='account_type_label'>
									Account Type
								</InputLabel>
								<Select
									labelId='account_type_label'
									id='account_type'
									name='account_type'
									value={inputs.account_type}
									onChange={handleInputChange}
									label='Account Type'
									autoWidth>
									<MenuItem value={'premium'}>
										Premium
									</MenuItem>
									<MenuItem value={'free'}>Free</MenuItem>
								</Select>
							</FormControl>
						</div>

						<Button
							variant='contained'
							color='secondary'
							size='large'
							disableElevation
							onClick={() => registerUser()}>
							Sign Up
						</Button>
					</form>
				</Paper>
			</div>
		</Layout>
	)
}

export default register
