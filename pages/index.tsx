import { useEffect, useState } from 'react'

import dynamic from 'next/dynamic'

import { useSelector, useDispatch } from 'react-redux'
import { loginUser, logoutUser } from '../redux/modules/user/actions'

const Layout = dynamic(() => import('../components/Layout'), {
	loading: () => <p>...</p>,
})
const Home = dynamic(() => import('../components/home/Home'), {
	loading: () => <p>...</p>,
})
const Dashboard = dynamic(() => import('../components/dashboard/Dashboard'), {
	loading: () => <p>...</p>,
})

import { TOKEN_STORAGE_KEY, USERNAME } from '../lib/consts'

const Index = () => {
	const user = useSelector((state: { user: any }) => state.user)
	const dispatch = useDispatch()

	useEffect(() => {
		const storedToken = localStorage.getItem(TOKEN_STORAGE_KEY)
		const storedUsername = localStorage.getItem(USERNAME)
		if (
			storedToken !== null &&
			storedToken.includes('Bearer') &&
			storedUsername !== null
		) {
			dispatch(loginUser())
		} else {
			dispatch(logoutUser())
		}
	}, [user.isLoggedIn])

	return <Layout>{user.isLoggedIn ? <Dashboard /> : <Home />}</Layout>
}

export default Index
