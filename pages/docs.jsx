import SwaggerUI from 'swagger-ui-react'

const docs = () => {
	return <SwaggerUI url="https://api.swaggerhub.com/apis/StockAPI/StockAPI/1.0.0-oas3" />
}

export default docs
