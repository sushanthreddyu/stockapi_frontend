import jwtDecode from 'jwt-decode'

import { TOKEN_STORAGE_KEY } from '../consts'

export type DecodedToken = {
	readonly email: string
	readonly exp: number
}

export class AuthToken {
	readonly decodedToken: DecodedToken

	constructor(readonly token?: string) {
		// we are going to default to an expired decodedToken
		this.decodedToken = { email: '', exp: 0 }

		// then try and decode the jwt using jwt-decode
		try {
			const jwtoken = token.replace('Bearer ', '')
			if (jwtoken) this.decodedToken = jwtDecode(jwtoken)
		} catch (e) {}
	}

	static async storeToken(token: string) {
		localStorage.setItem(TOKEN_STORAGE_KEY, token)
	}

	get authorizationString() {
		return `${this.token}`
	}

	get expiresAt(): Date {
		return new Date(this.decodedToken.exp * 1000)
	}

	get isExpired(): boolean {
		return new Date() > this.expiresAt
	}

	get isValid(): boolean {
		return !this.isExpired
	}
}
